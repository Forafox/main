#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define print_(type, x) type##_print(x)

void int64_t_print(int64_t i) { printf("%" PRId64 "\n", i); }
void double_print(double d) { printf("%lf\n", d); }

void error() {
  fprintf(stderr, "BAD\n");
  abort();
}

void print_newline() { puts(""); }

#define print(x)                                                               \
  _Generic((x), int64_t                                                        \
           : int64_t_print(x), double                                          \
           : double_print(x), default                                          \
           : error())



int main() {
  int64_t x = 42;
  double  d = 99.99;

  print(x);
  print(d);

  return 0;
}
